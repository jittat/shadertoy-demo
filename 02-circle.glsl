void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    // Time varying pixel color
    vec3 col = vec3(1,1,1);

    float r = 0.2;
    
    vec2 center = vec2(0.5 + cos(iTime)*r, 0.5 + sin(iTime)*r);
    
    float diff = smoothstep(0.5,0.8,length(center - uv)*10.0);
    
    col *= diff;
    col = clamp(col, 0.0, 1.0);

    // for taking other texture
    // vec3 finalCol = col * texture(iChannel0, uv).rgb;

    // Output to screen
    fragColor = vec4(col,1.0);
}
