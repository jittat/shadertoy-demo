void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = fragCoord/iResolution.xy;

    vec2 d = uv - vec2(0.5,0.5);
    float xx = length(d);
    float yy = atan(d.y/d.x) + iTime/2.0;
    
    float r = 0.4 + sin(iTime*2.0)/5.0;
    vec3 col = vec3(0,0.5,1.0 * smoothstep(r, r+0.3, xx + sin(yy*10.0)/2.0));

    // Output to screen
    fragColor = vec4(col,1.0);
}
